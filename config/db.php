<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=lesson_yii2_first',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix'=>'yii2_'

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
